====== Rubrique-à-Brac ======

  * Nombre de tomes : 7
  * Scénario : [[auteur:Gotlib]]
  * Dessins : [[auteur:Gotlib]]
  * Genre : [[genre:Comic strip]]
  * Registre : [[registre:comique]], [[registre:Satirique]]
  * Origine : [[origine:France]]
  * Éditeur : Dargaud
  * Rang : {{tagpage>rang2|2}}
  * Première édition : 1970

===== Liens =====
  * [[http://fr.wikipedia.org/wiki/Rubrique-%C3%A0-brac|Wikipédia]]
  * [[http://www.bedetheque.com/serie-1018-BD-Rubrique-a-Brac.html|BDgest']]


{{tag>BD scGotlib dessGotlib ComicStrip Comique Satirique France rang2}}
