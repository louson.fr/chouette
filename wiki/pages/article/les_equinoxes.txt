====== Les Équinoxes ======

  * Nombre de tomes : [[Genre:One shot]]
  * Scénario : [[auteur:Cyril Pedrosa]]
  * Dessins : [[auteur:Cyril Pedrosa]]
  * Genre : [[genre:Vie dans le Monde]]
  * Registre : [[registre:Réaliste]]
  * Thème : [[theme:Société]]
  * Origine : [[origine:France]]
  * Éditeur : Dupuis
  * Collection : [[collection:Aire Libre]]
  * Première édition : 2015

===== Liens =====
  * [[https://www.bedetheque.com/serie-48105-BD-Equinoxes.html|BDgest']]


{{tag>BD scCPedrosa dessCPedrosa Monde OneShot Realiste Societe France AireLibre rang4}}
