====== La guerre éternelle ======

La guerre éternelle est un [[genre:space opera]] adapté du roman de Joe Halderman.

  * Nombre de tomes : 3
  * Scénario : Joe Halderman, [[auteur:Marvano]]
  * Dessins : [[auteur:Marvano]]
  * Couleurs : Bruno Marchand
  * Genre : [[genre:Science-fiction]]
  * Origine : [[origine:Belgique]]
  * Éditeur : Dupuis
  * Collection : [[collection:Aire Libre]]
  * Rang : {{tagpage>rang3|3}}
  * Première édition : 1988

===== Liens =====
  * [[http://fr.wikipedia.org/wiki/La_Guerre_%C3%A9ternelle_%28bande_dessin%C3%A9e%29|Wikipédia]]
  * [[http://www.bedetheque.com/serie-233-BD-Guerre-eternelle.html|BDgest']]


{{tag>BD dessMarvano SpaceOpera SF Belgique AireLibre rang3 Bibli}}
