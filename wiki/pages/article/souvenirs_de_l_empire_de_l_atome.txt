====== Souvenirs de l'empire de l'atome ======

  * Nombre de tomes : [[genre:One shot]]
  * Scénario : [[auteur:Thierry Smolderen]]
  * Dessins : [[auteur:Alexandre Clérisse]]
  * Genre : [[genre:Science-fiction]]
  * Registre : [[registre:Dramatique]]
  * Thème : [[theme:Futur]]
  * Origine : [[origine:Europe]]
  * Éditeur : Dargaud
  * Première édition : 2013


===== Liens =====
  * [[http://www.bedetheque.com/serie-36576-BD-Souvenirs-de-L-empire-de-l-atome.html|BDgest']]


{{tag>BD scTSmolderen dessAClerisse OneShot SF Dramatique Futur Europe rang3 Bibli}}
