====== Shangri-La ======

Shangri-La, c'est du dessin de SF sans scénario. Il y a des plans incroyables, mais l'univers n'est qu'une parabole futuriste du notre.

  * Nombre de tomes : [[genre:One shot]]
  * Scénario : [[auteur:Mathieu Bablet]]
  * Dessins : [[auteur:Mathieu Bablet]]
  * Genre : [[genre:Science-Fiction]]
  * Registre : [[registre:dramatique]]
  * Thème : [[theme:politique]]
  * Origine : [[origine:France]]
  * Éditeur : Ankama Éditions
  * Collection : [[collection:Label 619]]
  * Rang : {{tagpage>rang5|5}}
  * Première édition : 2016

===== Liens =====
  * [[http://www.bedetheque.com/serie-53065-BD-Shangri-La.html|BDgest']]


{{tag>BD OneShot scMBablet dessMBablet SF Dramatique Politique France Label619 rang5}}
