====== Spirou et fantasio (saga de l'ingénu) ======

La saga de l'ingénu est la série initiée par Émile Bravo dans les créations originales des aventures de Spirou et Fantasio.

  * Nombre de tomes : 2
  * Scénario : [[auteur:Émile Bravo]], [[auteur:Yann]]
  * Dessins : [[auteur:Émile Bravo]], [[auteur:Olivier Schwartz]]
  * Couleurs : Rémi Chaurand, Delphine Chédru, Laurence Croix
  * Genre : [[genre:Aventure]]
  * Origine : [[origine:France]]
  * Éditeur : Dupuis
  * Rang : {{tagpage>rang1|1}}
  * Première édition : 2008

===== Liens =====
  * [[http://fr.wikipedia.org/wiki/Le_Journal_d%27un_ing%C3%A9nu|Wikipédia]]
  * [[http://www.bedetheque.com/serie-12999-BD-Spirou-et-Fantasio-Une-aventure-de.html|BDgest']]


{{tag>BD scEBravo scYann dessEBravo dessOSchwartz Aventure France rang1 Bibli}}
