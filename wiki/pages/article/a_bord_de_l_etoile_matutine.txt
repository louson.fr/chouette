====== À bord de L'Étoile Matutine ======

A bord de L'Étoile Matutine est une adaptation du [[http://fr.wikipedia.org/wiki/%C3%80_bord_de_L%27%C3%89toile_Matutine|roman]] de Pierre Mac Orlan.

  * Nombre de tomes : [[genre:One shot]]
  * Scénario : Pierre Mac Orlan, [[auteur:Riff Reb's]]
  * Dessins : [[auteur:Riff Reb's]]
  * Genre : [[genre:Aventure]], [[genre:Voyage en mer]]
  * Thème : [[theme:Pirates]]
  * Origine : [[origine:France]]
  * Éditeur : Soleil
  * Collection : [[collection:Noctambule]]
  * Rang : {{tagpage>rang2|2}}
  * Première édition : 2009

===== Liens =====
  * [[http://www.bedetheque.com/serie-20559-BD-A-bord-de-l-Etoile-Matutine.html|BDgest']]


{{tag>BD scRRebs dessRRebs OneShot Aventure Pirates VoyageEnMer France Noctambule rang3 Bibli}}
