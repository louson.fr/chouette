====== L'arabe du futur ======

  * Nombre de tomes : 5
  * Scénario : [[auteur:Riad Sattouf]]
  * Dessins : [[auteur:Riad Sattouf]]
  * Genre : [[genre:Biographie]], [[genre:Historique]], [[genre:Vie dans le Monde]]
  * Origine : [[origine:France]]
  * Éditeur : Allary Éditions
  * Rang : {{tagpage>rang2|2}}
  * Première édition : 

===== Liens =====
  * [[http://www.bedetheque.com/serie-43032-BD-Arabe-du-futur.html|BDgest']]
  * [[https://fr.wikipedia.org/wiki/L'Arabe_du_futur|Wikipedia]]


{{tag>BD scRSattouf dessRSattouf Biographie Historique Monde Voyages France rang2 Bibli}}
