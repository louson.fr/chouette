====== Ghost in the Shell ======

  * Nombre de tomes : 5
  * Scénario : [[auteur:Masamune Shirow]]
  * Dessins : [[auteur:Masamune Shirow]]
  * Genre : [[genre:Science-fiction]], [[genre:Cyberpunk]], [[genre:Action]]
  * Origine : [[origine:Japon]]
  * Éditeur : Glénat
  * Collection : [[collection:Akira]]
  * Rang : {{tagpage>rang1|1}}
  * Première édition : 1996

===== Liens =====
  * [[http://fr.wikipedia.org/wiki/Ghost_in_the_Shell|Wikipédia]]
  * [[http://www.bedetheque.com/serie-172-BD-Ghost-in-the-Shell.html|BDgest']]


{{tag>Manga scMShirow dessMShirow SF Cyberpunk Action Japon Akira rang1}}
