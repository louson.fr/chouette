====== Ama - Le souffle des femmes ======


  * Nombre de tomes : [[genre:One Shot]]
  * Scénario : [[auteur:Franck Manguin]]
  * Dessins : [[auteur:Cécile Becq]]
  * Genre : [[genre:Historique]]
  * Registre : [[registre:Réaliste]]
  * Thème : [[theme:Femmes]], [[theme:Japon]]
  * Éditeur : Sarbacane
  * Collection : [[collection:Sarbacane]]
  * Rang : {{tagpage>rang2|2}}
  * Sortie : 2020

===== Liens =====
  * [[https://www.bedetheque.com/serie-69936-BD-Ama-Le-souffle-des-femmes.html|BDgest']]


{{tag>BD scFManguin dessCBecq OneShot Historique Realiste Femmes themeJapon Sarbacane rang2 want}}
