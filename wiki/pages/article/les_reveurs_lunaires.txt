====== Les rêveurs lunaires ======

  * Nombre de tomes : [[genre:One shot]]
  * Scénario : [[auteur:Baudoin]], [[auteur:CVillani]]
  * Dessins : [[auteur:Baudoin]]
  * Genre : [[genre:Historique]]
  * Thème : [[theme:Sciences]]
  * Origine : [[origine:France]]
  * Éditeur : Gallimard/Grasset
  * Rang : {{tagpage>rang3|3}}
  * Première édition : 2015

===== Liens =====
  * [[http://www.bd.gallimard.fr/ouvrage-A66593-les_reveurs_lunaires.html|Gallimard]]
  * [[http://www.bedetheque.com/serie-47243-BD-Reveurs-lunaires.html|BDgest']]


{{tag>BD scBaudoin scCVillani dessBaudoin Historique Sciences France rang3 Bibli}}
