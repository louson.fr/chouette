====== Chroniques de la Lune Noire ======

  * Nombre de tomes : 14 (+3) +1 hors-série
  * Scénario : [[auteur:Froideval]]
  * Dessins : [[auteur:Olivier Ledroit]](1-5), [[auteur:Cyril Pontet]](6-14)
  * Genre : [[genre:Fantasy]], [[genre:Fantasy médiévale]]
  * Registre: [[registre:Baston]]
  * Origine : [[origine:France]]
  * Éditeur : Zenda
  * Collection : [[collection:Technicolor]]
  * Rang : {{tagpage>rang2|2}}
  * Première édition : 1989

===== Liens =====
  * [[http://fr.wikipedia.org/wiki/Chroniques_de_la_Lune_noire|Wikipédia]]
  * [[http://www.bedetheque.com/serie-61-BD-Chroniques-de-la-Lune-Noire.html|BDgest']]


{{tag>BD scFroideval dessOLedroit dessCPontet Fantasy regBaston FantasyMedievale France Technicolor rang2 Bibli}}
