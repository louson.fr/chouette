====== Ippo ======

Ippo est un manga de boxe.

  * Nombre de tomes : 105 (...)
  * Scénario : [[auteur:George Morikawa]]
  * Dessins : [[auteur:George Morikawa]]
  * Genre : [[genre:Shônen]], [[genre:Sport]]
  * Thèmes : [[theme:Boxe]]
  * Origine : [[origine:Japon]]
  * Éditeur : Kurokawa
  * Rang : {{tagpage>rang3|3}}
  * Première édition : 2010

===== Liens =====
  * [[http://fr.wikipedia.org/wiki/Ippo#Manga|Wikipédia]]
  * [[http://www.bedetheque.com/serie-24936-BD-Ippo-Destins-de-boxeurs.html|BDgest']]


{{tag>Manga scGMorikawa dessGMorikawa Shonen Sport Boxe Japon rang3 Bibli}}
