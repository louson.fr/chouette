====== Goupil ou face ======

Goupil ou face est une BD où l'auteure raconte sa [[https://fr.wikipedia.org/wiki/Cyclothymie|cyclothimie]].

  * Nombre de tomes : [[genre:One shot]]
  * Scénario : [[auteur:Lou Lubie]]
  * Genre : [[genre:Autobiographie]]
  * Registre : [[registre:Réaliste]]
  * Thème : [[theme:Maladies]]
  * Origine : [[origine:France]]
  * Éditeur : Vraoum !
  * Collection : [[collection:Vraoum !]]
  * Rang : {{tagpage>rang1|1}}
  * Première édition : 2016

===== Liens =====
  * [[http://www.goupil-ou-face.fr/|Goupil ou face]]
  * [[https://www.bdgest.com/chronique-7461-BD-Goupil-ou-face-Goupil-ou-face.html#review|BDgest']]


{{tag>BD scLLubie dessLLubie OneShot Autobiographie Realiste Maladies France Vraoum! rang1 Bibli}}
