====== Quai d'Orsay ======

  * Nombre de tomes : 2
  * Scénario : [[auteur:Christophe Blain]], [[auteur:Abel Lanzac]]
  * Dessins : [[auteur:Christophe Blain]]
  * Couleurs : Clémence Sapin
  * Genre : [[genre:Biographie]]
  * Thèmes : [[theme:Politique]]
  * Origine : [[origine:France]]
  * Éditeur : Dargaud
  * Rang : {{tagpage>rang3|3}}
  * Première édition : 2010

===== Liens =====
  * [[http://fr.wikipedia.org/wiki/Quai_d%27Orsay_%28bande_dessin%C3%A9e%29|Wikipédia]]
  * [[http://www.bedetheque.com/serie-24260-BD-Quai-d-Orsay.html|BDgest']]


{{tag>BD scCBlain scALanzac dessCBlain Biographie Politique France rang3}}
