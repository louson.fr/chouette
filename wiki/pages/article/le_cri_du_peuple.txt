====== Le cri du peuple ======

Le cri du peuple est une adaptation du roman de Jean Vautrin.

  * Nombre de tomes : 4
  * Scénario : Jean Vautrin, [[auteur:Jacques Tardi]]
  * Dessins : [[auteur:Jacques Tardi]]
  * Genre : [[genre:Historique]]
  * Origine : [[origine:France]]
  * Éditeur : Casterman
  * Rang : {{tagpage>rang3|3}}
  * Première édition : 2001

===== Liens =====
  * [[http://fr.wikipedia.org/wiki/Le_Cri_du_peuple_%28bande_dessin%C3%A9e%29|Wikipédia]]
  * [[http://www.bedetheque.com/serie-1562-BD-Cri-du-peuple.html|BDgest']]


{{tag>BD scJTardi dessJTardi Hisorique France rang3 Bibli}}
