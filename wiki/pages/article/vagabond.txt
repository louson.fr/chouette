====== Vagabond ======

Vagabond est l'adaptation du roman Musashi de Eiji Yoshikawa.

  * Nombre de tomes : 36 (en cours)
  * Scénario : [[auteur:Takehiko Inoue]]
  * Dessins : [[auteur:Takehiko Inoue]]
  * Genre : [[genre:Seinen]], [[genre:Baston]]
  * Thèmes : [[theme:Samourais]]
  * Origine : [[origine:Japon]]
  * Éditeur : Tonkam
  * Rang : {{tagpage>rang2|2}}
  * Première édition : 2001

===== Liens =====
  * [[http://fr.wikipedia.org/wiki/Vagabond_%28manga%29|Wikipédia]]
  * [[http://www.bedetheque.com/serie-5996-BD-Vagabond.html|BDgest']]


{{tag>Manga scTInoue dessTInoue Seinen Samourais Baston Japon rang2}}
