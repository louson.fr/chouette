====== Le réducteur de vitesse ======

  * Nombre de tomes : [[genre:One shot]]
  * Scénario : [[auteur:Christophe Blain]]
  * Dessins : [[auteur:Christophe Blain]]
  * Couleurs : Walter
  * Genre : [[genre:Aventure]], [[genre:Voyage en mer]]
  * Origine : [[origine:France]]
  * Éditeur : Dupuis
  * Collection : [[collection:Aire Libre]]
  * Rang : {{tagpage>rang3|3}}
  * Première édition : 1999

===== Liens =====
  * [[http://www.bedetheque.com/serie-3898-BD-Reducteur-de-vitesse.html|BDgest']]


{{tag>BD OneShot scCBlain dessCBlain Aventure VoyageEnMer France AireLibre rang3}}
