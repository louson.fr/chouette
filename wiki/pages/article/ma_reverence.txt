====== Ma révérence ======

  * Nombre de tomes : [[genre:One Shot]]
  * Scénario : [[auteur:Wilfrid Lupano]]
  * Dessins : [[auteur:Rodguen]]
  * Couleurs : Ohazar
  * Genre : [[genre:Policier]]
  * Registre : [[registre:Dramatique]]
  * Thème : [[theme:Braquage]]
  * Origine : [[origine:France]]
  * Éditeur : Delcourt
  * Collection : [[collection:Machination]]
  * Rang : {{tagpage>rang4|4}}
  * Première édition : 2013

===== Liens =====
  * [[http://www.bedetheque.com/serie-39347-BD-Ma-reverence.html|BDgest']]


{{tag>BD OneShot scWLupano dessRodguen Policier Dramatique Braquage France Machination rang4}}
