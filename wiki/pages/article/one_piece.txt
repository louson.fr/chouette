====== One piece ======

  * Nombre de tomes : 72 (en cours)
  * Scénario : [[auteur:Eiichirô Oda]]
  * Dessins : [[auteur:Eiichirô Oda]]
  * Genre : [[genre:Shônen]], [[genre:Aventure]], [[genre:Baston]]
  * Thème : [[theme:Pirates]]
  * Origine : [[origine:Japon]]
  * Éditeur : Glénat
  * Collection : [[collection:Shônen Manga]]
  * Rang : {{tagpage>rang1|1}}
  * Première édition : 2000

===== Liens =====
  * [[http://fr.wikipedia.org/wiki/One_Piece|Wikipédia]]
  * [[http://www.bedetheque.com/serie-4594-BD-One-Piece.html|BDgest']]
  * [[http://fr.onepiece.wikia.com/wiki/One_Piece_Encyclop%C3%A9die|Wikia]]


{{tag>Manga Shonen scEOda dessEOda Aventure Pirates Baston Japon ShonenManga rang1 Bibli}}
