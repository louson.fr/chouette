====== Le transperceneige ======

Le transperceneige est la BD qui a inspiré le film swowpiercer.

  * Nombre de tomes : [[genre:One shot]]
  * Scénario : [[auteur:Jacques Lob]]
  * Dessins : [[auteur:Jean-Marc Rochette]]
  * Genre : [[genre:Science-fiction]]
  * Registre : [[registre:Dramatique]]
  * Thème : [[theme:Apocalypse]]
  * Origine : [[origine:France]]
  * Éditeur : Casterman
  * Collection : [[collection:Les Romans]]
  * Rang : {{tagpage>rang4|4}}
  * Première édition : 2002

===== Liens =====
  * [[https://fr.wikipedia.org/wiki/Le_Transperceneige|Wikipédia]]
  * [[https://www.bedetheque.com/serie-395-BD-Transperceneige.html|BDgest']]


{{tag>BD OneShot scJLob dessJMRochette SF Dramatique Apocalypse France LesRomans rang4}}
