====== Bastard !!======

  * Nombre de tomes : 27 (en cours)
  * Scénario : [[auteur: Kazushi Hagiwara]]
  * Dessins : [[auteur:Kazushi Hagiwara]]
  * Genre : [[genre:Seinen]], [[genre:Fantasy]], [[genre:Baston]]
  * Origine : [[origine:Japon]]
  * Éditeur : Glénat
  * Collection : [[collection:Glénat Manga]]
  * Rang : {{tagpage>rang2|2}}
  * Première édition : 1996

===== Liens =====
  * [[http://fr.wikipedia.org/wiki/Bastard!!|Wikipédia]]
  * [[http://www.bedetheque.com/serie-4129-BD-Bastard.html|BDgest']]


{{tag>Manga scKHagiwara dessKHagiwara Seinen Fantasy Baston Japon GlenatManga rang2}}
