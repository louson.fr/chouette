====== Logicomix ======

Logicomix est un [[genre:one shot]] abordant les débuts de la logique.

  * Nombre de tomes : 1
  * Scénario : [[auteur:Apóstolos K. Doxiàdis]], Christos Papadimitriou
  * Dessins : [[auteur:Alecos Papadatos]]
  * Couleurs :  Annie Di Donna
  * Genre : [[genre:Vie dans le monde]]
  * Registre : [[registre:Réaliste]]
  * Thème : [[theme:Philosophie]]
  * Origine : [[origine:Grèce]]
  * Éditeur : Ikaros, Vuibert
  * Première édition : 2010

===== Liens =====
  * [[https://fr.wikipedia.org/wiki/Logicomix|Wikipédia]]
  * [[https://www.bedetheque.com/serie-24949-BD-Logicomix.html|BDgest']]


{{tag>Comic scAKDoxiadis dessAPapadatos OneShot Monde Realiste Philosophie Grece rang4 Bibli}}
