====== Café Budapest ======

  * Nombre de tomes : [[genre:One shot]]
  * Scénario : [[auteur:Alfonso Zapico]]
  * Dessins : [[auteur:Alfonso Zapico]]
  * Genre : [[genre:Vie dans le monde]], [[genre:Historique]]
  * Registre : [[registre:Dramatique]]
  * Thème : [[theme:Exil]]
  * Origine : [[origine:Espagne]]
  * Éditeur : Steinkis
  * Première édition : 2008

===== Liens =====
  * [[http://www.senscritique.com/bd/Cafe_Budapest/20093334|Sens critique]]
  * [[http://www.bdgest.com/chronique-7124-BD-Cafe-Budapest-Cafe-Budapest.html|BDgest']]


{{tag>BD OneShot scAZapico dessAZapico Monde Voyages Historique Dramatique Exil Espagne rang3 Bibli}}
