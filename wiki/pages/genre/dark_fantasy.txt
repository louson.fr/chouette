====== Dark fantasy ======

Sous-genre de la [[genre:Fantasy]]

===== Sous-genres =====
{{topic>DarkFantasy +SousGenre&nodate&nouser&sort}}

===== Bandes dessinées =====
{{topic>DarkFantasy +BD&nodate&nouser&sort}}

===== Mangas =====
{{topic>DarkFantasy +Manga&nodate&nouser&sort}}

{{tag>SousGenre Fantasy}}
