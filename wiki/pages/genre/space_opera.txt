====== Space opera (genre) ======

Space opera est un sous-genre de la science-fiction caractérisé par des histoires d'aventure épiques ou dramatiques se déroulant dans un cadre géopolitique complexe. Suivant les œuvres, le space opera rime avec exploration spatiale à grande échelle, guerres intergalactiques ou rigueur dans le réalisme scientifique. Apparu formellement au début des années 1940, le genre devient très populaire à partir des années 1960 et 1970 avec notamment Star Trek et Star Wars.

===== Bandes dessinées =====

{{topic>SpaceOpera +BD&nodate&nouser&sort}}


===== Mangas =====

{{topic>SpaceOpera +Manga&nodate&nouser&sort}}

===== Liens ======
[[http://fr.wikipedia.org/wiki/Space_opera|Wikipédia]]

{{tag>SF SousGenre}}