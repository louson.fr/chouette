====== Planet opera (genre) ======

Le planet opera est un sous-genre de science-fiction, parfois appelé romance planétaire.

===== Bandes dessinées =====
{{topic>PlanetOpera +BD&nodate&nouser&sort}}

===== Mangas =====
{{topic>PlanetOpera +Manga&nodate&nouser&sort}}

===== Liens =====
[[http://fr.wikipedia.org/wiki/Planet_opera|Wikipédia]]

{{tag>SF SousGenre}}
