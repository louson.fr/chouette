====== @!PAGE@ (genre) ======

===== Sous-genres =====
{{topic>@!PAGE@ +SousGenre&nodate&nouser&sort}}

===== Bandes dessinées =====
{{topic>@!PAGE@ +BD&nodate&nouser&sort}}

===== Mangas =====
{{topic>@!PAGE@ +Manga&nodate&nouser&sort}}

===== Comics =====
{{topic>@!PAGE@ +Comic&nodate&nouser&sort}}

{{tag>Genre}}
